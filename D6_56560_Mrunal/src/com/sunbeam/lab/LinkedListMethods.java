package com.sunbeam.lab;

public class LinkedListMethods {
	
	private Node head;
	private int nodesCount;
	
  public static class Node
  {
	  Box box;
	  private Node next;
	  
	public Node(Box box) {
		super();
		this.box = box;
		this.next = null;
	}
	  
	  
  }

public LinkedListMethods() {
	super();
	this.head = null;
	this.nodesCount = 0;
}

public int getNodescount() {
	return nodesCount;
}

public void setNodescount(int nodescount) {
	this.nodesCount = nodescount;
}
public boolean isListEmpty() {
	return (head==null);
}

public Node addNodeAtFirstPosition(Box box)
{
    Node newNode =new Node(box);
    if(isListEmpty()&& nodesCount<5)
    {
     head=newNode; 
     newNode.next=head; 
     nodesCount++;
    }
    else
    {
        Node trav=head;
        while(trav.next!=head)
        {
          trav=trav.next;
        }
        newNode.next=head;
        head=newNode;
        trav.next=head;
        nodesCount++;
    }
return head;
}

public Node deleteNodeAtFirstPosition() {
	
	if(!isListEmpty()) {
		if(head==head.next) {
			head=null;
			nodesCount--;
		}else
		{
			Node trav=head;
			while(trav.next!=head) 
				trav=trav.next;
				
			head=head.next;
			trav.next=head;
			nodesCount--;
			
		}
	}
	return head;
}
public boolean isFull() {
	int count=0;
	Node trav=this.head;
	do {
		++count;
		trav=trav.next;
	}while(trav!=head);
	return nodesCount>4;
}
  
}
