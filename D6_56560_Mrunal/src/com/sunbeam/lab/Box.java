package com.sunbeam.lab;

public class Box {

	private String b_color;
	private double b_height;
	private double b_width;
	private double b_weight;
	public Box(String b_color, double b_height, double b_width, double b_weight) {
		super();
		this.b_color = b_color;
		this.b_height = b_height;
		this.b_width = b_width;
		this.b_weight = b_weight;
	}
	public Box() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getB_color() {
		return b_color;
	}
	public void setB_color(String b_color) {
		this.b_color = b_color;
	}
	public double getB_height() {
		return b_height;
	}
	public void setB_height(double b_height) {
		this.b_height = b_height;
	}
	public double getB_width() {
		return b_width;
	}
	public void setB_width(double b_width) {
		this.b_width = b_width;
	}
	public double getB_weight() {
		return b_weight;
	}
	public void setB_weight(double b_weight) {
		this.b_weight = b_weight;
	}
	@Override
	public String toString() {
		return "Box [b_color=" + b_color + ", b_height=" + b_height + ", b_width=" + b_width + ", b_weight=" + b_weight
				+ "]";
	}
	
}
